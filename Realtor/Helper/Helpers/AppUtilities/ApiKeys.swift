//
//  ApiKeys.swift
//  Caviar
//
//  Created by YATIN  KALRA on 20/08/20.
//  Copyright © 2020 Ankur. All rights reserved.
//

import Foundation

enum ApiKey {
    static var code: String { return "CODE" }
    
    
    static var mobile : String { return "mobile" }
    static var email : String { return "email" }
    static var user_id : String { return "user_id" }
    static var friend_id : String { return "friend_id" }
    static var type : String { return "type" }
    static var name : String { return "name" }
    static var message : String { return "message" }
    static var favorite_status : String { return "favorite_status" }
    static var event_id : String { return "event_id" }
    static var country_code : String { return "country_code"}
    static var title : String { return "title"}
    static var body : String { return "body"}
    static var receipt : String { return "receipt"}
    
    static var sender : String { return "sender"}
    static var receiver : String { return "receiver"}
    static var media : String { return "media"}
    static var media_type : String { return "media_type"}
    
    static var fcm_token : String { return "fcm_token" }
    static var first_name : String { return "first_name" }
    static var last_name : String { return "last_name" }
    static var dob : String { return "dob" }
    static var birthdaypublic : String { return "birthdaypublic" }
    static var address : String { return "address" }
    static var lat : String { return "lat" }
    static var long : String { return "long" }
    static var bio : String { return "bio" }
    static var gender : String { return "gender" }
    static var interestedin : String { return "interestedin" }
 //   static var Instagram : String { return "Instagram" }
    static var Photo1 : String { return "Photo1" }
    static var Photo2 : String { return "Photo2" }
    static var Photo3 : String { return "Photo3" }
    static var Photo4 : String { return "Photo4" }
    static var Photo5 : String { return "Photo5" }
    static var height : String { return "height" }
    static var body_type : String { return "body_type" }
    static var smoke : String { return "smoke" }
    static var somke : String { return "somke" }
    static var drink : String { return "drink" }
    static var relationship : String { return "relationship" }
    static var kids : String { return "kids" }
    static var religion : String { return "religion" }
    static var ethinicity : String { return "ethinicity" }
    static var education : String { return "education" }
    static var Interests : String { return "Interests" }
    static var age_from : String { return "age_from" }
    static var age_to : String { return "age_to" }
    static var height_from : String { return "height_from" }
    static var height_to : String { return "height_to" }
    static var seeking_body_type : String { return "seeking_body_type" }
    static var seeking_smoke : String { return "seeking_smoke" }
    static var seeking_drink : String { return "seeking_drink" }
    static var seeking_relationship : String { return "seeking_relationship" }
    static var seeking_kids : String { return "seeking_kids" }
    static var seeking_religion : String { return "seeking_religion" }
    static var seeking_ethinicity : String { return "seeking_ethinicity" }
    static var seeking_education : String { return "seeking_education" }
    
    static var interests : String { return "interests" }
    static var location : String { return "location" }
    static var max_distance : String { return "max_distance" }
    static var allow_calender : String { return "allow_calender" }
    static var  block_user_id : String {return "block_user_id" }
    
    static var event_type : String {return "event_type" }
    static var confirmed : String {return "confirmed" }
    static var reg_type : String {return "reg_type" }
    
    static var lname : String {return "lname" }
    static var latitude : String {return "latitude" }
    static var longitude : String {return "longitude" }
    static var instagram : String {return "instagram" }

    static var living_in : String {return "living_in" }
    static var job_title : String {return "job_title" }
    static var company : String {return "company" }
    static var profile_image : String {return "profile_image" }
    static var this_week : String {return "this_week" }
    static var this_weekend : String {return "this_weekend" }
    static var tomorrow : String {return "tomorrow" }
    static var next_3hour : String {return "next_3hour" }
    static var today : String {return "today" }
    static var event_lat : String {return "event_lat" }
    static var event_long : String {return "event_long" }
    
    static var event_date_from  : String {return "event_date_from"}
    static var event_date_to  : String {return "event_date_to"}
    static var location_radius_in_miles  : String {return "location_radius_in_miles"}
    static var interested_in : String {return "interested_in"}
    static var verified_themselves  : String {return "verified_themselves"}
    static var advance_height_from  : String {return "advance_height_from"}
    static var advance_height_to  : String {return "advance_height_to"}
    static var advance_body_type  : String {return "advance_body_type"}
    static var advance_education  : String {return "advance_education"}
    static var advance_drink  : String {return "advance_drink"}
    static var advance_smoke  : String {return "advance_smoke"}
    static var advance_religion  : String {return "advance_religion"}
    static var advance_relationship  : String {return "advance_relationship"}
    static var advance_children  : String {return "advance_children"}
    
//    static var friend_id  : String {return "friend_id"}
    
    
    
    
}
//MARK:- Api Code
//=======================
enum ApiCode {
    
    static var success: Int { return 200 } // Success
    static var unauthorizedRequest: Int { return 206 } // Unauthorized request
    static var headerMissing: Int { return 207 } // Header is missing
    static var phoneNumberAlreadyExist: Int { return 208 } // Phone number alredy exists
    static var requiredParametersMissing: Int { return 418 } // Required Parameter Missing or Invalid
    static var fileUploadFailed: Int { return 421 } // File Upload Failed
    static var pleaseTryAgain: Int { return 500 } // Please try again
    static var tokenExpired: Int { return 401 } // Token expired refresh token needed to be generated
    
}

//MARK:- Api Response
//=======================
enum ApiResponse {

    static var response: Bool { return true } // response
   
}
