//
//  UserDetail.swift
//  holidayshare
//
//  Created by Preeti Dhankar on 01/04/18.
//  Copyright © 2018 Preeti Dhankar. All rights reserved.
//

import UIKit

class UserDetail: NSObject {
    
static let shared = UserDetail()
    private override init() { }
    
    func setUserId(_ sUserId:String) -> Void {
        UserDefaults.standard.set(sUserId, forKey: UserKeys.user_id.rawValue)
    }
    func getUserId() -> String {
        if let userId = UserDefaults.standard.value(forKey: UserKeys.user_id.rawValue) as? String
        {
            return userId
        }
        return ""
    }
    func removeUserId() -> Void {
        UserDefaults.standard.removeObject(forKey: UserKeys.user_id.rawValue)
    }
    var userID:String? {
        return  UserDefaults.standard.value(forKey: UserKeys.user_id.rawValue) as? String
    }
}

enum UserKeys:String {
    case user_id = "user_id"
    case allowCalendar = "allowCalendar"
}

