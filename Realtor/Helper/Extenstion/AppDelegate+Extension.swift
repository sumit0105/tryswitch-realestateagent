//
//  AppDelegate+Extension.swift
//  BalvahanApp
//
//  Created by subesh pandey on 06/11/20.
//

import Foundation
import UIKit

@available(iOS 13.0, *)
extension AppDelegate {
    
    // MARK: - App Delegate Ref
    class func delegate() -> AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    class func getTopViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {

           if let nav = base as? UINavigationController {
               return getTopViewController(base: nav.visibleViewController)

           } else if let tab = base as? UITabBarController, let selected = tab.selectedViewController {
               return getTopViewController(base: selected)

           } else if let presented = base?.presentedViewController {
               return getTopViewController(base: presented)
           }
           return base
       }
}
