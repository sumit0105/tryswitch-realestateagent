//
//  BaseViewController.swift
//  MVCSwift
//
//  Created by Kiyara Tomar on 29/03/17.
//  Copyright © 2017 Vcareall. All rights reserved.
//

//import UIKit
////import SDWebImage
////import ActionSheetPicker_3_0
//import SVProgressHUD
//
//class BaseViewController: UIViewController {
//
////    let navTitleAttrs = [
////        NSAttributedString.Key.foregroundColor: UIColor.darkGray,
////        NSAttributedString.Key.font: UIFont(name: "Montserrat-SemiBold", size: 16)!
////    ]
//    var noTouchBGView : UIView?
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//    }
//
//    static let shared: BaseViewController = {
//            let instance = BaseViewController()
//            // Setup code
//            return instance
//        }()
//
//
//    override func didReceiveMemoryWarning() {
//        super.didReceiveMemoryWarning()
//        // Dispose of any resources that can be recreated.
//    }
//    //To show messages in APP
//
//    func showMessage(_ titleStr: String ,_ messageStr: String) {
//        let alert = UIAlertController(title:titleStr, message:messageStr , preferredStyle: UIAlertController.Style.alert)
//        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
//        self.present(alert, animated: true, completion: nil)
//    }
//
//
//    func showInputDialog(title:String? = nil,
//                         subtitle:String? = nil,
//                         actionTitle:String? = "Ok",
//                         cancelTitle:String? = "Cancel",
//                         inputPlaceholder:String? = nil,
//                         inputKeyboardType:UIKeyboardType = UIKeyboardType.default,
//                         cancelHandler: ((UIAlertAction) -> Swift.Void)? = nil,
//                         actionHandler: ((_ text: String?) -> Void)? = nil) {
//
//        let alert = UIAlertController(title: title, message: subtitle, preferredStyle: .alert)
//        alert.addTextField { (textField:UITextField) in
//            textField.placeholder = inputPlaceholder
//            textField.keyboardType = inputKeyboardType
//        }
//        alert.addAction(UIAlertAction(title: actionTitle, style: .destructive, handler: { (action:UIAlertAction) in
//            guard let textField =  alert.textFields?.first else {
//                actionHandler?(nil)
//                return
//            }
//            actionHandler?(textField.text)
//        }))
//        alert.addAction(UIAlertAction(title: cancelTitle, style: .cancel, handler: cancelHandler))
//
//        self.present(alert, animated: true, completion: nil)
//    }
//
//    func addRefreshControl(_ tableView: UITableView, selector: Selector, andTintColor color: UIColor = .darkGray) {
//        let refreshControl = UIRefreshControl()
//        refreshControl.addTarget(self, action:
//            selector, for: .valueChanged)
//        refreshControl.tintColor = color
//
//        tableView.addSubview(refreshControl)
//    }
//
////   @objc func openPolicySideMenu() {
////    let storyboard = UIStoryboard(name: "PolicySideMenu", bundle: nil)
////    let vc = storyboard.instantiateViewController(withIdentifier: "PolicySideMenuVC")
////    let navigation = SideMenuNavigationController(rootViewController: vc)
////
////    navigation.leftSide = true
////    navigation.menuWidth = UIScreen.main.bounds.width - 90
////    navigation.presentationStyle = .menuSlideIn
////    navigation.statusBarEndAlpha = 0
//////    navigation.presentationStyle.backgroundColor = UIColor.lightGray
////
////    SideMenuManager.default.leftMenuNavigationController = navigation
//////        SideMenuManager.default.menuWidth = UIScreen.main.bounds.width - 90
//////        SideMenuManager.default.menuFadeStatusBar = false
//////        SideMenuManager.default.menuPresentMode = .menuSlideIn
////
////    present(SideMenuManager.default.leftMenuNavigationController ?? UINavigationController(), animated: true, completion: nil)
////    }
//
////   @objc func openPolicySideMenu() {
////        let storyboard = UIStoryboard(name: "PolicySideMenu", bundle: nil)
////        let vc = storyboard.instantiateViewController(withIdentifier: "PolicySideMenuVC")
////        let navigation = UISideMenuNavigationController(rootViewController: vc)
////        navigation.leftSide = true
//////        navigation.navigationBar.barTintColor = UIColor(red: 198.0/255, green: 198.0/255, blue: 200.0/255, alpha: 0.15)
////        SideMenuManager.default.menuLeftNavigationController = navigation
////        SideMenuManager.default.menuWidth = UIScreen.main.bounds.width - 90
////        SideMenuManager.default.menuFadeStatusBar = false
////        SideMenuManager.default.menuPresentMode = .menuSlideIn
////        present(SideMenuManager.default.menuLeftNavigationController ?? UINavigationController(), animated: true, completion: nil)
////    }
//
////    @objc func openPolicySideMenu() {
////        let storyboard = UIStoryboard(name: "SideMenu", bundle: nil)
////        let vc = storyboard.instantiateViewController(withIdentifier: "SideMenu")
////        let navigation = UISideMenuNavigationController(rootViewController: vc)
////        navigation.leftSide = true
////        navigation.navigationBar.barTintColor = .red//UIColor(red: 198.0/255, green: 198.0/255, blue: 200.0/255, alpha: 0.15)
////        navigation.navigationBar.isHidden = true
//////navigation.interactivePopGestureRecognizer?.isEnabled = false
////        SideMenuManager.default.menuLeftNavigationController = navigation
////        SideMenuManager.default.menuWidth = UIScreen.main.bounds.width - 90
////        SideMenuManager.default.menuFadeStatusBar = false
////        SideMenuManager.default.menuPresentMode = .menuSlideIn
////        present(SideMenuManager.default.menuLeftNavigationController ?? UINavigationController(), animated: true, completion: nil)
////    }
//
//    @IBAction func globalBackBtn(_ sender:UIBarButtonItem){
//        _ = navigationController?.popViewController(animated: true)
//    }
//
//    // mark temp use
////    @IBAction func rightNavBtn(_ sender:UIBarButtonItem){
////        let aboutUsHelpVC = self.storyboard?.instantiateViewController(withIdentifier: AboutUsHelp_Screen) as! AboutUsHelpController
////        aboutUsHelpVC.vcType = .Help
////        self.navigationController?.pushViewController(aboutUsHelpVC, animated: true)
////    }
//
//    func setBottomLinetoTextFeild( textField:UITextField){
//        let border = CALayer()
//        let width = CGFloat(0.5)
//        border.borderColor = UIColor.darkGray.cgColor
//        if (self.view.frame.size.width==320) {
//            border.frame = CGRect(x: 0, y: textField.frame.size.height - width, width: textField.frame.size.width, height: 1)
//        }
//        else{
//            border.frame = CGRect(x: 0, y: textField.frame.size.height - width, width: textField.frame.size.width+50, height: 1)
//        }
//        border.borderWidth = width
//        textField.layer.addSublayer(border)
//        textField.layer.masksToBounds = true
//
//    }
//
//    func showProgressDialog(_ view: UIView, message : String) {
//
//        BaseViewController.shared.noTouchBGView = UIView()
//        noTouchBGView?.backgroundColor = .lightText
//        BaseViewController.shared.view.addSubview(BaseViewController.shared.noTouchBGView ?? UIView())
//
//        BaseViewController.shared.noTouchBGView?.translatesAutoresizingMaskIntoConstraints = false
//        BaseViewController.shared.noTouchBGView?.topAnchor.constraint(equalTo:  BaseViewController.shared.view.topAnchor).isActive = true
//        BaseViewController.shared.noTouchBGView?.leftAnchor.constraint(equalTo:  BaseViewController.shared.view.leftAnchor).isActive = true
//        BaseViewController.shared.noTouchBGView?.rightAnchor.constraint(equalTo: BaseViewController.shared.view.rightAnchor).isActive = true
//        BaseViewController.shared.noTouchBGView?.bottomAnchor.constraint(equalTo: BaseViewController.shared.view.bottomAnchor).isActive = true
//
//        SVProgressHUD.setContainerView(view)
//        SVProgressHUD.show(withStatus: message)
//    }
//
//    func hideProgressDialog(_ message : String){
//        BaseViewController.shared.noTouchBGView?.clearConstraints()
//        BaseViewController.shared.noTouchBGView?.removeFromSuperview()
//        SVProgressHUD.dismiss(withDelay: 0.30)
//        SVProgressHUD.showSuccess(withStatus: message)
//    }
//    func showAlertForNil(massage:String){
//        PopUpActionViewController.showPopup(parentVC: self, statuslbl: massage, img: #imageLiteral(resourceName: "errorimg"), btnBacground: .black, btnTitleDs: "OK", actionBool: false)
//    }
//    func hexStringToUIColor (hex:String) -> UIColor {
//        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
//
//        if (cString.hasPrefix("#")) {
//            cString.remove(at: cString.startIndex)
//        }
//
//        if ((cString.count) != 6) {
//            return UIColor.gray
//        }
//
//        var rgbValue:UInt64 = 0
//        Scanner(string: cString).scanHexInt64(&rgbValue)
//
//        return UIColor(
//            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
//            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
//            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
//            alpha: CGFloat(1.0)
//        )
//    }
//    func ShowLoader (vc:UIViewController){
//    let alert = UIAlertController(title: nil, message: "Please wait...", preferredStyle: .alert)
//
//    let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
//
//    loadingIndicator.hidesWhenStopped = true
//
//        loadingIndicator.style = UIActivityIndicatorView.Style.medium
//    loadingIndicator.startAnimating();
//        alert.view.backgroundColor = #colorLiteral(red: 0.1294117647, green: 0.1294117647, blue: 0.1294117647, alpha: 1)
//    alert.view.addSubview(loadingIndicator)
//
//        vc.present(alert, animated: true, completion: nil)
//
//    }
//
//         func hideLoader (vc:UIViewController) {         if let vc = vc.presentedViewController, vc is UIAlertController {             dismiss(animated: false, completion: nil)         }     }
//
//
//    }
//
//
//
