//
//  Constant.swift
//  MVCSwift3
//
//  Created by Kiyara Tomar on 30/03/17.
//  Copyright © 2017 IOSTeamVcareall. All rights reserved.
//

import Foundation
import UIKit


let kGOOGLEPLACEAPIKEY = "AIzaSyAG5yWZ4F-wBcOn6I1ci-PUl-kgt012Zm4"
let BASEURL = "http://inmortaltech.com/Inspecto-APIs/"
let ImageURL = "http://inmortaltech.com/Inspecto-APIs/assets/userServiceImage/"
let ImageURL2 = "http://inmortaltech.com/Inspecto-APIs/assets/inquiryImage/"

enum SubUrl: String {
    
    case USER_LOGIN = "login"
    case USER_Registeration = "customerRegisteration"
    case Search_workshop = "search"
    case Time_list = "listTime"
    case Booking_Resheduled = "UserReschedule"
    case Get_Vehicle_Detail = "getVehicleDetails"
    case VEHICLE_List = "listVehicle"
    case Manufacturer_list = "listManufacturer"
    case Model_list = "listModel"
    case Manufacture_year_list = "listYear"
    case Model_version_list = "listVersion"
    case Service_list = "listService"
    case  Forget_Password = "forgotPassword"
    case GET_verify = "mobileVerified"
    case User_Add_Vehicle = "userAddVehicle"
    case New_Password = "newPassword"
    case User_Booking_list = "getUserService"
    case User_Enquary_list = "getInquery"
    case Email_Verification = "userDetails"
    case User_Enquary_Bidlist = "InquiryBidList"
    case User_Accept_Bid = "UserAcceptBid"
    case User_Vehicle_list = "getUserVehicle"
    case User_Adress_list = "userAddress"
    case User_Add_Adress = "addAddress"
    case User_spareCategory_list = "spareCategory"
    case User_subCategory_list = "spareSubCategory"
    case User_subWorkshop_list = "subWorkshop"
    case User_userFavourite = "userFavourite"
    case User_GetFeedback = "getFeedback"
    case User_WorkshopAmenitues = "listAminities"
    case User_SelectService = "selectService"
    case DropDown_Filter_Data = "getprice"
    case User_Vin_Data = "VinNumber"
    
    case GET_OTP = "sendOtp"
    case GET_Email_OTP = "emailVerified"
    case Help = ""
    case About = "company.html"
    case Privacy = "privacy-policy.html"
    case TermsCondition = "terms-and-conditions.html"


    var url: String {
        return "\(BASEURL)\(self.rawValue)"
    }
   
//    var wbaseurl: String {
//        return "\()\(self.rawValue)"
//    }
}


let Main_Storyboard_Screen = "Main"
let Login_Screen = "NewLoginResistrationVc"
let VerifyOTP_Screen = "verifyotpview"
let Sidemenu_Screen = "sidemenu"
let Dashboard_Screen = "dashboardview"
let Profile_Screen = "ProfileVc"

enum AppMessages : String {
    // Login
    case Val_Login_TF = "Please enter email"
    case Val_Mobile_TF = "Please enter mobile"
    case Val_Password_TF = "Password"
    case Alert_Login_TF = "Please enter mobile or email"
    case Alert_Mobile_Email_TF = "Please enter valid mobile or email"
    case Alert_Mobile_TF = "Please enter mobile "
    case Alert_OTP_TF = "Please enter OTP "
    case Alert_UserName = "Please enter full name"
    case Alert_Paasword_TF = "Please enter password"
    case Alert_SignUp_TF = "Please enter First name"
    
    case Mobile_TF = "08123456789"
    case Search_TF = "Search Location"
    case Vehilce_ModelName_TF = "Vehicle Model Name*"
    case Vehilce_ModelNumber_TF = "Vehicle Model Number*"
    case Password_not_Mactched   = "Password not matched"
    
}
  

enum UserRolesType : String {
    case Admin = "Admin"
    case User = "user"
    case Parent = "parent"
    case Teacher = "teacher"

}

struct Font {
        
        // *** Montserrat *** //

        struct Style {
            
            
            static var Franklin_Book_Regular = "Franklin-Gothic-Book-Regular"
            static var Franklin_Medium = "FranklinGothic-Medium"
            static var Montserrat_Black = "Montserrat-Black"
            static var Montserrat_BlackItalic = "Montserrat-BlackItalic"
            static var Montserrat_Bold = "Montserrat-Bold"
            static var Montserrat_BoldItalic = "Montserrat-BoldItalic"
            static var Montserrat_ExtraLight = "Montserrat-ExtraLight"
            static var Montserrat_ExtraLightItalic = "Montserrat-ExtraLightItalic"
            static var Montserrat_Italic = "Montserrat-Italic"
            static var Montserrat_Light = "Montserrat-Light"
            static var Montserrat_LightItalic = "Montserrat-LightItalic"
            static var Montserrat_MediumItalic = "Montserrat-MediumItalic"
            static var Montserrat_Regular = "Montserrat-Regular"
            static var Montserrat_Medium = "Montserrat-Medium"
            static var Montserrat_SemiBold = "Montserrat-SemiBold"
            static var Montserrat_SemiBoldItalic = "Montserrat-SemiBoldItalic"
            static var Montserrat_Thin = "Montserrat-Thin"
            static var Montserrat_ThinItalic = "Montserrat-ThinItalic"

            
            // *** Helvetica *** //
            
            static var Helvetica = "Helvetica"
            static var HelveticaBold = "Helvetica-Bold"
            static var HelveticaBoldOblique = "Helvetica-BoldOblique"
            static var HelveticaLight = "Helvetica-Light"
            static var HelveticaLightOblique = "Helvetica-LightOblique"
            static var HelveticaOblique = "Helvetica-Oblique"
            
            //*** Helvetica Neue ***//
            static var HelveticaNeue = "HelveticaNeue"
            static var HelveticaNeueBold = "HelveticaNeue-Bold"
            static var HelveticaNeueBoldItalic = "HelveticaNeue-BoldItalic"
            static var HelveticaNeueCondensedBlack = "HelveticaNeue-CondensedBlack"
            static var HelveticaNeueCondensedBold = "HelveticaNeue-CondensedBold"
            static var HelveticaNeueItalic = "HelveticaNeue-Italic"
            
            static var HelveticaNeueLight = "HelveticaNeue-Light"
            static var HelveticaNeueLightItalic = "HelveticaNeue-LightItalic"
            static var HelveticaNeueMedium = "HelveticaNeue-Medium"
            static var HelveticaNeueMediumItalic = "HelveticaNeue-MediumItalic"
            static var HelveticaNeueThin = "HelveticaNeue-Thin"
            static var HelveticaNeueThinItalic = "HelveticaNeue-ThinItalic"
            static var HelveticaNeueUltraLight = "HelveticaNeue-UltraLight"
            static var HelveticaNeueUltraLightItalic = "HelveticaNeue-UltraLightItalic"
            
            //*** Coustm font ***//
            static var Helveticaneuew23forsky = "helveticaneuew23forsky-reg"
            static var Helveticaneuew23fosky = "helveticaneuew23fosky-bd"
            
        }
        
        struct FontSize {
            static var five =  5.0
            static var six =  6.0
            static var seven =  7.0
            static var eight =  8.0
            static var nine =  9.0
            static var ten =  10.0
            static var eleven =  11.0
            static var twelve =  12.0
            static var thirteen =  13.0
            static var fourteen =  14.0
            static var fifteen =  15.0
            static var sixteen =  16.0
            static var seventeen =  17.0
            static var eighteen =  18.0
            static var ninteen =  19.0
            static var twenty =  20.0
            static var twentyone =  21.0
            static var twentytwo =  22.0
            static var twentythree =  23.0
            static var twentyfour =  24.0
            static var twentyfive =  25.0
            static var twentyeight =  28.0

            
        }
        
        static var franklin_Book_23 = UIFont(name: Style.Franklin_Book_Regular, size: 23)
        static var franklin_Medium_33 = UIFont(name: Style.Franklin_Medium, size: 33)

    
        static var montserrat_Medium_12 = UIFont(name: Style.Montserrat_Medium, size: CGFloat(FontSize.twelve))
        static var montserrat_Medium_13 = UIFont(name: Style.Montserrat_Medium, size: CGFloat(FontSize.thirteen))


        static var montserrat_Medium_base = UIFont(name: Style.Montserrat_Medium, size: CGFloat(FontSize.fourteen))
        
        static var montserrat_Regular_base = UIFont(name: Style.Montserrat_Regular, size: CGFloat(FontSize.fourteen))

        static var montserrat_SemiBold_base = UIFont(name: Style.Montserrat_SemiBold, size: CGFloat(FontSize.fourteen))


        
        static var helvetica10 = UIFont(name: Style.Helvetica, size: CGFloat(FontSize.ten))
        static var helvetica11 = UIFont(name: Style.Helvetica, size: CGFloat(FontSize.eleven))
        static var helvetica12 = UIFont(name: Style.Helvetica, size: CGFloat(FontSize.twelve))
        static var helvetica13 = UIFont(name: Style.Helvetica, size: CGFloat(FontSize.thirteen))
        static var helvetica14 = UIFont(name: Style.Helvetica, size: CGFloat(FontSize.fourteen))
        static var helvetica15 = UIFont(name: Style.Helvetica, size: CGFloat(FontSize.fifteen))
        
        static var helveticaBold10 = UIFont(name: Style.HelveticaBold, size: CGFloat(FontSize.ten))
        static var helveticaBold11 = UIFont(name: Style.HelveticaBold, size: CGFloat(FontSize.eleven))
        static var helveticaBold12 = UIFont(name: Style.HelveticaBold, size: CGFloat(FontSize.twelve))
        static var helveticaBold13 = UIFont(name: Style.HelveticaBold, size: CGFloat(FontSize.thirteen))
        static var helveticaBold14 = UIFont(name: Style.HelveticaBold, size: CGFloat(FontSize.fourteen))
        static var helveticaBold15 = UIFont(name: Style.HelveticaBold, size: CGFloat(FontSize.fifteen))
        
        static var helveticaBoldOblique10 = UIFont(name: Style.HelveticaBold, size: CGFloat(FontSize.ten))
        static var helveticaBoldOblique11 = UIFont(name: Style.HelveticaBoldOblique, size: CGFloat(FontSize.eleven))
        static var helveticaBoldOblique12 = UIFont(name: Style.HelveticaBoldOblique, size: CGFloat(FontSize.twelve))
        static var helveticaBoldOblique13 = UIFont(name: Style.HelveticaBoldOblique, size: CGFloat(FontSize.thirteen))
        static var helveticaBoldOblique14 = UIFont(name: Style.HelveticaBoldOblique, size: CGFloat(FontSize.fourteen))
        static var helveticaBoldOblique15 = UIFont(name: Style.HelveticaBoldOblique, size: CGFloat(FontSize.fifteen))
        
        
        
        
        static var systemRegular10 = UIFont.systemFont(ofSize: CGFloat(FontSize.ten), weight: .regular)
        static var systemRegular11 = UIFont.systemFont(ofSize: CGFloat(FontSize.eleven), weight: .regular)
        static var systemRegular12 = UIFont.systemFont(ofSize: CGFloat(FontSize.twelve), weight: .regular)
        static var systemRegular13 = UIFont.systemFont(ofSize: CGFloat(FontSize.thirteen), weight: .regular)
        static var systemRegular14 = UIFont.systemFont(ofSize: CGFloat(FontSize.fourteen), weight: .regular)
        static var systemRegular15 = UIFont.systemFont(ofSize: CGFloat(FontSize.fifteen), weight: .regular)
        static var systemRegular16 = UIFont.systemFont(ofSize: CGFloat(FontSize.sixteen), weight: .regular)
        static var systemRegular17 = UIFont.systemFont(ofSize: CGFloat(FontSize.seventeen), weight: .regular)
        
        static var systemBold9 = UIFont.systemFont(ofSize: CGFloat(FontSize.nine), weight: .bold)
        
        static var systemBold10 = UIFont.systemFont(ofSize: CGFloat(FontSize.ten), weight: .bold)
        static var systemBold11 = UIFont.systemFont(ofSize: CGFloat(FontSize.eleven), weight: .bold)
        static var systemBold12 = UIFont.systemFont(ofSize: CGFloat(FontSize.twelve), weight: .bold)
        static var systemBold13 = UIFont.systemFont(ofSize: CGFloat(FontSize.thirteen), weight: .bold)
        static var systemBold14 = UIFont.systemFont(ofSize: CGFloat(FontSize.fourteen), weight: .bold)
        static var systemBold15 = UIFont.systemFont(ofSize: CGFloat(FontSize.fifteen), weight: .bold)
        static var systemBold16 = UIFont.systemFont(ofSize: CGFloat(FontSize.sixteen), weight: .bold)
        static var systemBold17 = UIFont.systemFont(ofSize: CGFloat(FontSize.seventeen), weight: .bold)
        static var systemBold20 = UIFont.systemFont(ofSize: CGFloat(FontSize.twenty), weight: .bold)
        static var systemBold25 = UIFont.systemFont(ofSize: CGFloat(FontSize.twentyfive), weight: .bold)
        
        
        
        static var systemBlack10 = UIFont.systemFont(ofSize: CGFloat(FontSize.ten), weight: .black)
        static var systemBlack11 = UIFont.systemFont(ofSize: CGFloat(FontSize.eleven), weight: .black)
        static var systemBlack12 = UIFont.systemFont(ofSize: CGFloat(FontSize.twelve), weight: .black)
        static var systemBlack13 = UIFont.systemFont(ofSize: CGFloat(FontSize.thirteen), weight: .black)
        static var systemBlack14 = UIFont.systemFont(ofSize: CGFloat(FontSize.fourteen), weight: .black)
        static var systemBlack15 = UIFont.systemFont(ofSize: CGFloat(FontSize.fifteen), weight: .black)
        static var systemBlack16 = UIFont.systemFont(ofSize: CGFloat(FontSize.sixteen), weight: .black)
        static var systemBlack17 = UIFont.systemFont(ofSize: CGFloat(FontSize.seventeen), weight: .black)
        
        static var systemMedium10 = UIFont.systemFont(ofSize: CGFloat(FontSize.ten), weight: .medium)
        static var systemMedium11 = UIFont.systemFont(ofSize: CGFloat(FontSize.eleven), weight: .medium)
        static var systemMedium12 = UIFont.systemFont(ofSize: CGFloat(FontSize.twelve), weight: .medium)
        static var systemMedium13 = UIFont.systemFont(ofSize: CGFloat(FontSize.thirteen), weight: .medium)
        static var systemMedium14 = UIFont.systemFont(ofSize: CGFloat(FontSize.fourteen), weight: .medium)
        static var systemMedium15 = UIFont.systemFont(ofSize: CGFloat(FontSize.fifteen), weight: .medium)
        static var systemMedium16 = UIFont.systemFont(ofSize: CGFloat(FontSize.sixteen), weight: .medium)
        static var systemMedium17 = UIFont.systemFont(ofSize: CGFloat(FontSize.seventeen), weight: .medium)
        
        static var systemHeavy10 = UIFont.systemFont(ofSize: CGFloat(FontSize.ten), weight: .heavy)
        static var systemHeavy11 = UIFont.systemFont(ofSize: CGFloat(FontSize.eleven), weight: .heavy)
        static var systemHeavy12 = UIFont.systemFont(ofSize: CGFloat(FontSize.twelve), weight: .heavy)
        static var systemHeavy13 = UIFont.systemFont(ofSize: CGFloat(FontSize.thirteen), weight: .heavy)
        static var systemHeavy14 = UIFont.systemFont(ofSize: CGFloat(FontSize.fourteen), weight: .heavy)
        static var systemHeavy15 = UIFont.systemFont(ofSize: CGFloat(FontSize.fifteen), weight: .heavy)
        static var systemHeavy16 = UIFont.systemFont(ofSize: CGFloat(FontSize.sixteen), weight: .heavy)
        static var systemHeavy17 = UIFont.systemFont(ofSize: CGFloat(FontSize.seventeen), weight: .heavy)
        
        static var systemLight10 = UIFont.systemFont(ofSize: CGFloat(FontSize.ten), weight: .light)
        static var systemLight11 = UIFont.systemFont(ofSize: CGFloat(FontSize.eleven), weight: .light)
        static var systemLight12 = UIFont.systemFont(ofSize: CGFloat(FontSize.twelve), weight: .light)
        static var systemLight13 = UIFont.systemFont(ofSize: CGFloat(FontSize.thirteen), weight: .light)
        static var systemLight14 = UIFont.systemFont(ofSize: CGFloat(FontSize.fourteen), weight: .light)
        static var systemLight15 = UIFont.systemFont(ofSize: CGFloat(FontSize.fifteen), weight: .light)
        static var systemLight16 = UIFont.systemFont(ofSize: CGFloat(FontSize.sixteen), weight: .light)
        static var systemLight17 = UIFont.systemFont(ofSize: CGFloat(FontSize.seventeen), weight: .light)
        
        static var systemSemiBold10 = UIFont.systemFont(ofSize: CGFloat(FontSize.ten), weight: .semibold)
        static var systemSemiBold11 = UIFont.systemFont(ofSize: CGFloat(FontSize.eleven), weight: .semibold)
        static var systemSemiBold12 = UIFont.systemFont(ofSize: CGFloat(FontSize.twelve), weight: .semibold)
        static var systemSemiBold13 = UIFont.systemFont(ofSize: CGFloat(FontSize.thirteen), weight: .semibold)
        static var systemSemiBold14 = UIFont.systemFont(ofSize: CGFloat(FontSize.fourteen), weight: .semibold)
        static var systemSemiBold15 = UIFont.systemFont(ofSize: CGFloat(FontSize.fifteen), weight: .semibold)
        static var systemSemiBold16 = UIFont.systemFont(ofSize: CGFloat(FontSize.sixteen), weight: .semibold)
        static var systemSemiBold17 = UIFont.systemFont(ofSize: CGFloat(FontSize.seventeen), weight: .semibold)
        
        static var systemThin10 = UIFont.systemFont(ofSize: CGFloat(FontSize.ten), weight: .thin)
        static var systemThin11 = UIFont.systemFont(ofSize: CGFloat(FontSize.eleven), weight: .thin)
        static var systemThin12 = UIFont.systemFont(ofSize: CGFloat(FontSize.twelve), weight: .thin)
        static var systemThin13 = UIFont.systemFont(ofSize: CGFloat(FontSize.thirteen), weight: .thin)
        static var systemThin14 = UIFont.systemFont(ofSize: CGFloat(FontSize.fourteen), weight: .thin)
        static var systemThin15 = UIFont.systemFont(ofSize: CGFloat(FontSize.fifteen), weight: .thin)
        static var systemThin16 = UIFont.systemFont(ofSize: CGFloat(FontSize.sixteen), weight: .thin)
        static var systemThin17 = UIFont.systemFont(ofSize: CGFloat(FontSize.seventeen), weight: .thin)
        
        static var systemUltraLihgt10 = UIFont.systemFont(ofSize: CGFloat(FontSize.ten), weight: .ultraLight)
        static var systemUltraLihgt11 = UIFont.systemFont(ofSize: CGFloat(FontSize.eleven), weight: .ultraLight)
        static var systemUltraLihgt12 = UIFont.systemFont(ofSize: CGFloat(FontSize.twelve), weight: .ultraLight)
        static var systemUltraLihgt13 = UIFont.systemFont(ofSize: CGFloat(FontSize.thirteen), weight: .ultraLight)
        static var systemUltraLihgt14 = UIFont.systemFont(ofSize: CGFloat(FontSize.fourteen), weight: .ultraLight)
        static var systemUltraLihgt15 = UIFont.systemFont(ofSize: CGFloat(FontSize.fifteen), weight: .ultraLight)
        static var systemUltraLihgt16 = UIFont.systemFont(ofSize: CGFloat(FontSize.sixteen), weight: .ultraLight)
        static var systemUltraLihgt17 = UIFont.systemFont(ofSize: CGFloat(FontSize.seventeen), weight: .ultraLight)
        
        static var systemFont10 = UIFont.systemFont(ofSize: CGFloat(FontSize.ten))
        static var systemFont11 = UIFont.systemFont(ofSize: CGFloat(FontSize.eleven))
        static var systemFont12 = UIFont.systemFont(ofSize: CGFloat(FontSize.twelve))
        static var systemFont13 = UIFont.systemFont(ofSize: CGFloat(FontSize.thirteen))
        static var systemFont14 = UIFont.systemFont(ofSize: CGFloat(FontSize.fourteen))
        static var systemFont15 = UIFont.systemFont(ofSize: CGFloat(FontSize.fifteen))
        static var systemFont16 = UIFont.systemFont(ofSize: CGFloat(FontSize.sixteen))
        static var systemFont17 = UIFont.systemFont(ofSize: CGFloat(FontSize.seventeen))
        static var systemFont18 = UIFont.systemFont(ofSize: CGFloat(FontSize.eighteen))
        static var systemFont19 = UIFont.systemFont(ofSize: CGFloat(FontSize.ninteen))
        static var systemFont20 = UIFont.systemFont(ofSize: CGFloat(FontSize.twenty))
     
    }
    








