//
//  CommonApi.swift
//  QuizAdda
//
//  Created by Yogendra Singh on 15/03/18.
//  Copyright © 2018 Yogendra Singh. All rights reserved.
//

//import UIKit
//import Alamofire
//import SwiftyJSON
////import CryptoSwift
//
//class CommonApi: NSObject {
//    
//    //To request get URL
//    class  func requestGETURL(_ strURL: String, success:@escaping (JSON) -> Void, failure:@escaping (Error) -> Void)
//    {
//        //            CommonMethods.showHud()
//        Alamofire.request(strURL,headers:nil).responseJSON { (responseObject) -> Void in
//            switch(responseObject.result)
//            {
//                case .success(_):
//                    //                    CommonMethods.hideHud()
//                    let resJson = JSON(responseObject.result.value!)
//                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.50) {
//                        success(resJson)
//                    }
//                    break
//                    
//                case .failure(_):
//                    //                    CommonMethods.hideHud()
//                    let error : Error = responseObject.result.error!
//                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.50) {
//                        failure(error)
//                    }
//                    break
//            }
//        }
//        
//    }
//    
//    class  func requestGETURLWithAcessToken(_ strURL: String, success:@escaping (JSON) -> Void, failure:@escaping (Error) -> Void)
//    {
////        let Acesstoken = ProjectUtils.getAcesstoken()
//        
////        let Auth = String(format:"%@ %@","Bearer",Acesstoken as CVarArg)
////        print(Auth)
//        let headers = ["token":"zsd16xzv3jsytnp87tk7ygv73k8zmr0ekh6ly7mxaeyeh46oe8"]
//        //        CommonMethods.showHud()
//        Alamofire.request(strURL,headers:headers).responseJSON { (responseObject) -> Void in
//            switch(responseObject.result)
//            {
//                case .success(_):
//                    //                CommonMethods.hideHud()
//                    let resJson = JSON(responseObject.result.value!)
//                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.50) {
//                        success(resJson)
//                    }
//                    break
//                    
//                case .failure(_):
//                    //                CommonMethods.hideHud()
//                    let error : Error = responseObject.result.error!
//                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.50) {
//                        failure(error)
//                    }
//                    break
//            }
//        }
//    }
//    
//    
//    //   requestPostCall
//    
//    //To request POST URL
//    class func requestPostCall(_ strUrl:String,CheckBool:Bool, method:HTTPMethod,parameter:Parameters,encoding:JSONEncoding , success:@escaping(JSON) -> Void, failure:@escaping(Error) -> Void) -> Void
//    {
//        
//        //let headers = ["Content-Type":"application/json"]
//        //"Content-Type":"application/json"
//        if CheckBool == true{
//            // original this header
//            // let headers = ["auth-token": ProjectUtils.getAcesstoken()]
//            
//            /////  Start   headers updated by Lokendra in this type request
////            let Acesstoken = ProjectUtils.getAcesstoken()
////            let Auth = String(format:"%@ %@","Bearer",Acesstoken as CVarArg)
//            //            print(Auth)
//            
//            //"Content-Type":"application/json"
//            
//            let headers = ["token":"zsd16xzv3jsytnp87tk7ygv73k8zmr0ekh6ly7mxaeyeh46oe8"]
//            /////// end
//            
//            Alamofire.request(strUrl, method: .post, parameters: parameter, encoding: URLEncoding.default, headers: headers).responseJSON { response in
//                
//                switch(response.result)
//                {
//                    case .success(_):
//                        let resJson = JSON(response.result.value!)
//                        success(resJson)
//                        break
//                        
//                    case .failure(_):
//                        let error : Error = response.result.error!
//                        failure(error)
//                        break
//                }
//            }
//        }else{
//            Alamofire.request(strUrl, method: .post, parameters: parameter, encoding: URLEncoding.default, headers: nil).responseJSON { response in
//                
//                switch(response.result)
//                {
//                    case .success(_):
//                        let resJson = JSON(response.result.value!)
//                        success(resJson)
//                        break
//                        
//                    case .failure(_):
//                        let error : Error = response.result.error!
//                        failure(error)
//                        break
//                }
//            }
//        }
//        
//    }
//    
//    
//    
//    
//    //    class func requestPostRawDataCall(strUrl:String, parameter:[String: Any], success:@escaping(JSON) -> Void, failure:@escaping(Error) -> Void) -> Void {
//    //
//    //
//    ////        let jsonNewString: String? = CommonMethods.convertDictionaryToString(parameter)
//    //        //        let encoder = JSONEncoder()
//    //        //        if let jsonData = try? encoder.encode(parameter) {
//    //        //            if let jsonString = String(data: jsonData, encoding: .utf8) {
//    //        //                print(">>>>>>" + jsonString)
//    //        //                jsonNewString = jsonString
//    //        //            }
//    //        //        }
//    //
//    //        let jsonDataa = jsonNewString?.data(using: .utf8, allowLossyConversion: false)!
//    //
//    //        let url = URL(string: strUrl)
//    //        var request = URLRequest(url: url!)
//    //        request.httpMethod = "POST"
//    //        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
//    //        request.httpBody = jsonDataa
//    //
//    //        Alamofire.request(request).responseJSON { response in
//    //            print("<<<<<<<response")
//    //            switch(response.result){
//    //                case .success(_):
//    //                    let resJson = JSON(response.result.value!)
//    //                    success(resJson)
//    //                    break
//    //
//    //                case .failure(_):
//    //                    let error : Error = response.result.error!
//    //                    failure(error)
//    //                    break
//    //            }
//    //        }
//    //    }
//    
//    class func requestPostCallWithAcessToken(_ strUrl:String, method:HTTPMethod,parameter:Parameters,encoding:JSONEncoding , success:@escaping(JSON) -> Void, failure:@escaping(Error) -> Void) -> Void
//    {
//        //        CommonMethods.showHud()
//        //        let Acesstoken = ProjectUtils.getAcesstoken()
//        //
//        //        let Auth = String(format:"%@ %@","Bearer",Acesstoken as CVarArg)
//        //        print(Auth)
//        let headers = ["token":"zsd16xzv3jsytnp87tk7ygv73k8zmr0ekh6ly7mxaeyeh46oe8"]
//        
//        Alamofire.request(strUrl, method: .post, parameters: parameter, encoding: URLEncoding.default, headers: headers).responseJSON { response in
//            
//            switch(response.result)
//            {
//                case .success(_):
//                    //                CommonMethods.hideHud()
//                    let resJson = JSON(response.result.value!)
//                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.50) {
//                        success(resJson)
//                    }
//                    break
//                    
//                case .failure(_):
//                    //                CommonMethods.hideHud()
//                    let error : Error = response.result.error!
//                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.50) {
//                        failure(error)
//                    }
//                    break
//            }
//        }
//    }
//    
//    
//    class func requestPostRawDataCall(strUrl:String, parameter:[String: Any], success:@escaping(JSON) -> Void, failure:@escaping(Error) -> Void) -> Void {
//        
//        
//        let jsonNewString: String? = CommonMethods.convertDictionaryToString(parameter)
//        //        let encoder = JSONEncoder()
//        //        if let jsonData = try? encoder.encode(parameter) {
//        //            if let jsonString = String(data: jsonData, encoding: .utf8) {
//        //                print(">>>>>>" + jsonString)
//        //                jsonNewString = jsonString
//        //            }
//        //        }
//        
//        let jsonDataa = jsonNewString?.data(using: .utf8, allowLossyConversion: false)!
//        
//        let url = URL(string: strUrl)
//        var request = URLRequest(url: url!)
//        request.httpMethod = "POST"
//        
//        request.setValue("100415fa75e78dc65f49168cf14001", forHTTPHeaderField: "x-client-id")
//        request.setValue("45ae28e76ffdcb20bfbf68a79299d5a3dba4ab5f", forHTTPHeaderField: "x-client-secret")
//        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
//        request.httpBody = jsonDataa
//        
//        Alamofire.request(request).responseJSON { response in
//            print("<<<<<<<response")
//            switch(response.result){
//                case .success(_):
//                    let resJson = JSON(response.result.value!)
//                    success(resJson)
//                    break
//                    
//                case .failure(_):
//                    let error : Error = response.result.error!
//                    failure(error)
//                    break
//            }
//        }
//    }
//    
//    
//  
//    
//    
//    
//    class func requestMultipart(uploadImage: UIImage?,strUrl: String?,_ imageName:String?, parameters: Parameters , progressOfImage: @escaping(Double) -> Void, success: @escaping(JSON) -> Void, failure: @escaping(Error) -> Void) {
//        print("Param: \(parameters)")
//        
//        guard let uploadImage = uploadImage, let strUrl = strUrl, let imageName = imageName else {
//            return
//        }
//        
//            Alamofire.upload(multipartFormData: { multipartFormData in
//                
//                multipartFormData.append(uploadImage.jpegData(compressionQuality:1)!, withName: "image", fileName: "\(imageName).jpeg", mimeType: "image/jpeg")
//                
//                
//                //    multipartFormData.append(("" as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: "")
//                
//                for (key, value) in parameters {
//                    multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
//                    
//                }
//                
//            }
//            , to: strUrl, method: .post, headers : nil,
//            encodingCompletion: { encodingResult in
//                switch encodingResult {
//                case .success(let upload, _, _):
//                    print(upload.progress)
//                    
//                    upload.responseJSON { response in
//                        print(response.result)
//                        if (response.result.isSuccess) {
//                            switch(response.result) {
//                            case .success(_):
//                                let resJson = JSON(response.result.value!)
//                                success(resJson)
//                                break
//                                
//                            case .failure(_):
//                                let error : Error = response.result.error!
//                                failure(error)
//                                break
//                            }
//                        }
//                        
//                        if (response.result.isFailure) {
//                            //let error = Error!.self
//                            //failure(error as! Error)
//                            let error: Error = response.result.error!
//                            failure(error)
//                        }
//                    }
//                case .failure(let encodingError):
//                    print(encodingError.localizedDescription)
//                    failure(encodingError.localizedDescription as! Error)
//                }
//            })
//        }
//    func uploadImage(isUser:Bool, endUrl: String, imageData: Data?, parameters: [String : Any],success: @escaping(JSON) -> Void, failure: @escaping(Error) -> Void){
//
////        headerFile()
//        let headers = ["token":"zsd16xzv3jsytnp87tk7ygv73k8zmr0ekh6ly7mxaeyeh46oe8"]
//       
//        Alamofire.upload(multipartFormData: { multipartFormData in
//            
//            for (key, value) in parameters {
//                if let temp = value as? String {
//                    multipartFormData.append(temp.data(using: .utf8)!, withName: key)
//                }
//                if let temp = value as? Int {
//                    multipartFormData.append("\(temp)".data(using: .utf8)!, withName: key)
//                }
//                if let temp = value as? NSArray {
//                    temp.forEach({ element in
//                        let keyObj = key + "[]"
//                        if let string = element as? String {
//                            multipartFormData.append(string.data(using: .utf8)!, withName: keyObj)
//                        } else
//                            if let num = element as? Int {
//                                let value = "\(num)"
//                                multipartFormData.append(value.data(using: .utf8)!, withName: keyObj)
//                        }
//                    })
//                }
//            }
//            
//            if let data = imageData{
//                multipartFormData.append(data, withName: "file", fileName: "\(Date.init().timeIntervalSince1970).png", mimeType: "image/png")
//            }
//        },
//        to: endUrl, method: .post , headers: nil, encodingCompletion:{ encodingResult in
//            switch encodingResult {
//            case .success(let upload, _, _):
//                print(upload.progress)
//                
//                upload.responseJSON { response in
//                    print(response.result)
//                    if (response.result.isSuccess) {
//                        switch(response.result) {
//                        case .success(_):
//                            let resJson = JSON(response.result.value!)
//                           success(resJson)
//                            break
//                            
//                        case .failure(_):
//                            let error : Error = response.result.error!
//                            failure(error)
//                            break
//                        }
//                    }
//                    
//                    if (response.result.isFailure) {
//                        //let error = Error!.self
//                        //failure(error as! Error)
//                        let error: Error = response.result.error!
//                        failure(error)
//                    }
//                }
//            case .failure(let encodingError):
//                print(encodingError.localizedDescription)
//                failure(encodingError.localizedDescription as! Error)
//            }
//        }
//       
//         )
//    
//      }
//
//    
//
//
///*
// 
// class APIManager {
// static let request = APIManager()
// 
// func serverCall(baseURL:String = BASEURL, apiName:String, params: [String : Any], httpMethod:String, receivedResponse:@escaping (_ succeeded:Bool, _ response:[String:Any]) -> ()) {
// print(baseURL + apiName)
// print(params ?? "")
// //    let loaderView = LoaderView.getLoader()
// //    UIApplication.shared.keyWindow?.addSubview(loaderView)
// if NetworkReachability.isConnectedToNetwork() == true {
// if let vc = UIApplication.shared.keyWindow?.rootViewController?.children.last {
// CommonMethods.showHud(aView: vc.view, userInteractionAllowed: false)
// DispatchQueue.main.asyncAfter(deadline: .now() + 20) {
// CommonMethods.hideHud(aView: vc.view)
// }
// }
// 
// let urlString = apiName.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
// var request = URLRequest(url: URL(string: (baseURL) + urlString!)!)
// if(httpMethod == "POST" || httpMethod == "PUT") {
// 
// request.httpBody = try! JSONSerialization.data(withJSONObject: params, options: [])
// //                   if isZipped == false {
// request.addValue("application/json", forHTTPHeaderField: "Content-Type")
// //                   } else {
// //                       request.addValue("application/octet-stream", forHTTPHeaderField: "Content-Type")
// //                       request.addValue("application/octet-stream", forHTTPHeaderField: "Content-Encoding: gzip")
// //                   }
// request.addValue("application/json", forHTTPHeaderField: "Accept")
// } else if(httpMethod == "GET") {
// if params.count > 0 {
// var paramString = ""
// for (key,value) in params {
// if key == params.first?.key {
// paramString += "?"
// } else {
// paramString += "&"
// }
// paramString += "\(key)=\(value)"
// }
// request = URLRequest(url: URL(string: (baseURL) + urlString! + paramString)!)
// }
// }
// //               if let token = UserDefaults.standard.value(forKey: USER_DEFAULT.accessToken) {
// //                   request.setValue("bearer \(token)", forHTTPHeaderField: "authorization")
// //               }
// request.httpMethod = httpMethod as String
// request.timeoutInterval = 20
// print(request)
// 
// let task = URLSession.shared.dataTask(with: request) {data, response, error in
// DispatchQueue.main.async {
// if let vc = UIApplication.shared.keyWindow?.rootViewController?.children.last {
// CommonMethods.hideHud(aView: vc.view)
// }
// }
// if(response != nil && data != nil) {
// do {
// if let json = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? [String:Any] {
// receivedResponse(true, json)
// } else {
// let jsonStr = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)    // No error thrown, but not NSDictionary
// let error = "Unable to show json" as NSString
// print("Error could not parse JSON: '\(jsonStr ?? error)'")
// receivedResponse(false, [:])
// }
// } catch let parseError {
// print(parseError)
// let jsonStr = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
// let error = "Unable to show json" as NSString
// print("Error could not parse JSON: '\(jsonStr ?? error)'")
// receivedResponse(false, [:])
// }
// } else {
// receivedResponse(false, [:])
// }
// }
// task.resume()
// } else {
// //        loaderView.removeFromSuperview()
// receivedResponse(false, ["message":"No internet connection"])
// }
// }
// 
// func symptomCheckerCall(apiName:String, params: [String : Any], httpMethod:String, receivedResponse:@escaping (_ succeeded:Bool, _ response:[[String:Any]]) -> ()) {
// let baseURL = "https://api.infermedica.com/v2/"
// print(baseURL + apiName)
// print(params)
// //    let loaderView = LoaderView.getLoader()
// //    UIApplication.shared.keyWindow?.addSubview(loaderView)
// if NetworkReachability.isConnectedToNetwork() == true {
// //            CommonMethods.showHud()
// let urlString = apiName.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
// var request = URLRequest(url: URL(string: (baseURL) + urlString!)!)
// if(httpMethod == "POST" || httpMethod == "PUT") {
// request.httpBody = try! JSONSerialization.data(withJSONObject: params, options: [])
// request.addValue("application/json", forHTTPHeaderField: "Content-Type")
// } else if(httpMethod == "GET") {
// if params.count > 0 {
// var paramString = ""
// for (key,value) in params {
// if key == params.first?.key {
// paramString += "?"
// } else {
// paramString += "&"
// }
// paramString += "\(key)=\(value)"
// }
// request = URLRequest(url: URL(string: (baseURL) + urlString! + paramString)!)
// }
// }
// request.setValue("4eb1d3a2", forHTTPHeaderField: "app-id")
// request.setValue("0032394801c5c304ddc208f9761b46ff", forHTTPHeaderField: "app-key")
// //               if let token = UserDefaults.standard.value(forKey: USER_DEFAULT.accessToken) {
// //                   request.setValue("bearer \(token)", forHTTPHeaderField: "authorization")
// //               }
// request.httpMethod = httpMethod as String
// request.timeoutInterval = 35
// print(request)
// 
// let task = URLSession.shared.dataTask(with: request) {data, response, error in
// //                CommonMethods.hideHud()
// if(response != nil && data != nil) {
// do {
// if let jsonArray = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? [[String:Any]] {
// receivedResponse(true, jsonArray)
// } else {
// let jsonStr = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)    // No error thrown, but not NSDictionary
// let error = "Unable to show json" as NSString
// print("Error could not parse JSON: '\(jsonStr ?? error)'")
// receivedResponse(false, [[:]])
// }
// } catch let parseError {
// print(parseError)
// let jsonStr = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
// let error = "Unable to show json" as NSString
// print("Error could not parse JSON: '\(jsonStr ?? error)'")
// receivedResponse(false, [[:]])
// }
// } else {
// receivedResponse(false, [[:]])
// }
// }
// task.resume()
// } else {
// //        loaderView.removeFromSuperview()
// receivedResponse(false, [["message":"No internet connection"]])
// }
// }
// 
// func symptomCheckerObjectCall(apiName:String, params: [String : Any], httpMethod:String, receivedResponse:@escaping (_ succeeded:Bool, _ response:[String:Any]) -> ()) {
// let baseURL = "https://api.infermedica.com/v2/"
// print(baseURL + apiName)
// print(params)
// //    let loaderView = LoaderView.getLoader()
// //    UIApplication.shared.keyWindow?.addSubview(loaderView)
// if NetworkReachability.isConnectedToNetwork() == true {
// //            CommonMethods.showHud()
// let urlString = apiName.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
// var request = URLRequest(url: URL(string: (baseURL) + urlString!)!)
// if(httpMethod == "POST" || httpMethod == "PUT") {
// request.httpBody = try! JSONSerialization.data(withJSONObject: params, options: [])
// request.addValue("application/json", forHTTPHeaderField: "Content-Type")
// } else if(httpMethod == "GET") {
// if params.count > 0 {
// var paramString = ""
// for (key,value) in params {
// if key == params.first?.key {
// paramString += "?"
// } else {
// paramString += "&"
// }
// paramString += "\(key)=\(value)"
// }
// request = URLRequest(url: URL(string: (baseURL) + urlString! + paramString)!)
// }
// }
// request.setValue("4eb1d3a2", forHTTPHeaderField: "app-id")
// request.setValue("0032394801c5c304ddc208f9761b46ff", forHTTPHeaderField: "app-key")
// //               if let token = UserDefaults.standard.value(forKey: USER_DEFAULT.accessToken) {
// //                   request.setValue("bearer \(token)", forHTTPHeaderField: "authorization")
// //               }
// request.httpMethod = httpMethod as String
// request.timeoutInterval = 35
// print(request)
// 
// let task = URLSession.shared.dataTask(with: request) {data, response, error in
// //                CommonMethods.hideHud()
// if(response != nil && data != nil) {
// do {
// if let json = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? [String:Any] {
// receivedResponse(true, json)
// } else {
// let jsonStr = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)    // No error thrown, but not NSDictionary
// let error = "Unable to show json" as NSString
// print("Error could not parse JSON: '\(jsonStr ?? error)'")
// receivedResponse(false, [:])
// }
// } catch let parseError {
// print(parseError)
// let jsonStr = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
// let error = "Unable to show json" as NSString
// print("Error could not parse JSON: '\(jsonStr ?? error)'")
// receivedResponse(false, [:])
// }
// } else {
// receivedResponse(false, [:])
// }
// }
// task.resume()
// } else {
// //        loaderView.removeFromSuperview()
// receivedResponse(false, ["message":"No internet connection"])
// }
// }
// }
// 
// 
// public extension Collection {
// 
// func jsonFormat() -> String {
// do {
// let jsonData = try JSONSerialization.data(withJSONObject: self, options: [.prettyPrinted])
// guard let jsonString = String(data: jsonData, encoding: String.Encoding.utf8) else {
// print("Can't create string with data.")
// return "{}"
// }
// return jsonString
// } catch let parseError {
// print("json serialization error: \(parseError)")
// return "{}"
// }
// }
// 
// }
// */
//}
