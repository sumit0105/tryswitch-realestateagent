//
//  EventBottomVc.swift
//  Realtor
//
//  Created by Anubhav on 02/11/21.
//

import UIKit
import FittedSheets

class EventBottomVc: UIViewController, Demoable {
    static var name: String = {"hgcycyt"}()
    static let identifier = "EventBottomVc"
    
    @IBOutlet weak var disCribeTV: UITextView!
    var shedule:()->() = {}
//    @IBOutlet weak var tf1: TextField!
    @IBOutlet var border_Tf: [UITextField]!
    override func viewDidLoad() {
        super.viewDidLoad()
        for tf in border_Tf{
            tf.setBorderColor(borderColor: .gray, cornerRadiusBound: 20)
            
        }
        let imageview = UIImageView(image: UIImage(named: "18"))
        imageview.contentMode = .scaleToFill
        let rightPadding: CGFloat = 2 //--- change right padding
        imageview.frame = CGRect(x: 0, y: 0, width: 18 + rightPadding , height:18)
//
        

//         tf1.rightViewMode = .always
//        tf1.rightView = imageview

        

        // set the rightView UIView as the textField's rightView
        disCribeTV.setBorderColor(borderColor: .gray, cornerRadiusBound: 5)
        
    }
    static func openDemo(from parent: UIViewController, in view: UIView?) {
        let useInlineMode = view != nil
        
        let controller = UIStoryboard(name: "SignUp", bundle: nil).instantiateViewController(withIdentifier: "EventBottomVc") as! EventBottomVc
        controller.shedule = {
            let vc = EventDetailVc.instantiate(fromAppStoryboard: .SignUp)
            
            parent.navigationController?.pushViewController(vc, animated: true)
        
        }
        let options = SheetOptions(
            useFullScreenMode: false,
            useInlineMode: useInlineMode)
        let sheet = SheetViewController(controller: controller, sizes: [.percent(0.6),.fullscreen], options: options)
//        sheet.hasBlurBackground = true
        sheet.overlayView.isOpaque = true
        sheet.gripSize = CGSize(width: 150, height: 8)
        sheet.minimumSpaceAbovePullBar = 100
        sheet.pullBarBackgroundColor = .white
//        sheet.dismissOnPull = false
        sheet.dismissOnOverlayTap = false
        addSheetEventLogging(to: sheet)
        
        if let view = view {
            sheet.animateIn(to: view, in: parent)
        } else {
            parent.present(sheet, animated: true, completion: nil)
        }
    }
    

    @IBAction func Shedule(_ sender: UIButton) {
        shedule()
    }
}

