//
//  EventVc.swift
//  Realtor
//
//  Created by Anubhav on 02/11/21.
//

import UIKit
import FSCalendar
class EventVc: UIViewController {

    
    @IBOutlet weak var calender: FSCalendar!
    @IBOutlet weak var table:UITableView!
    @IBOutlet weak var scroll: UIScrollView!
    @IBOutlet weak var tableh : NSLayoutConstraint!
    var date = "Nov 10, 2021"
    @IBOutlet weak var pastBtn: UIButton!
    
    @IBOutlet weak var invitClinentVie: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        table.delegate = self
        table.dataSource = self
        calender.delegate = self
        let gesture = UITapGestureRecognizer(target: self, action:  #selector (inviteAction (_:)))
        invitClinentVie.isUserInteractionEnabled = true
        invitClinentVie.addGestureRecognizer(gesture)
        pastBtn.setBorderColor(borderColor: UIColor.init(red: 245, green: 66, blue: 8), cornerRadiusBound: 20)
        self.table.register(UINib(nibName: "EventCell", bundle: nil), forCellReuseIdentifier: "EventCell")
    }
    @objc func inviteAction(_ sender:UITapGestureRecognizer){
       
       }
    @IBAction func btn_HomeTap(_ sender: UIButton) {
       
    }
    fileprivate let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM d, yyyy"
        return formatter
    }()
    
    @IBAction func btn_BackTap(_ sender: UIButton){
        self.tabBarController?.selectedIndex = 4
    }
        
    @IBAction func btnShedule(_ sender: Any) {
        EventBottomVc.openDemo(from: self, in: self.view)
        
       
    }
    @IBAction func btnNext(_ sender: Any) {
        calender.setCurrentPage(getNextMonth(date: calender.currentPage), animated: true)
    }
    @IBAction func btnBack(_ sender: Any) {
        calender.setCurrentPage(getPreviousMonth(date: calender.currentPage), animated: true)
    }
    
    func getNextMonth(date:Date)->Date {
        return  Calendar.current.date(byAdding: .month, value: 1, to:date)!
    }

    func getPreviousMonth(date:Date)->Date {
        return  Calendar.current.date(byAdding: .month, value: -1, to:date)!
    }
}

extension EventVc :UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 8
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//
////        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 30))
////        headerView.backgroundColor = .clear
////        let label = UILabel()
////        label.frame = CGRect.init(x: 0, y: 0, width: headerView.frame.width-20, height: headerView.frame.height - 10)
////
//////        label.text = "Other Products sold by Robert Adler"
////        label.text = "Autres produits/services par Robert Adler"
////
////        label.textColor = UIColor.black
////        label.font = UIFont(name: AppFonts.Roboto_Medium.rawValue, size: 14.0)
////
////        headerView.addSubview(label)
//
////        return headerView
//    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = table.dequeueReusableCell(withIdentifier: "EventCell", for: indexPath) as! EventCell
        cell.date.text = self.date
        cell.layer.cornerRadius = 10
        if indexPath.row % 2 == 0 {
            cell.backgroundColor = UIColor.init(red: 237, green: 237, blue: 237)
        }else{
            cell.backgroundColor = UIColor.init(red: 255, green: 255, blue: 255)
        }
        self.updateHight(table: table)
        
        cell.detailBtn.addTarget(self, action: #selector(payBtnSelection(_:)), for: .touchUpInside)
        return cell
    }
    @objc func payBtnSelection(_ sender:UIButton){
        
     let selectedIndexPath =  sender.tag
        print(selectedIndexPath)
        
        
            let vc = EventDetailVc.instantiate(fromAppStoryboard: .SignUp)
            
            self.navigationController?.pushViewController(vc, animated: true)
        
    }
        


    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row % 2 != 0 {
            
        }
    }
    
    func updateHight(table:UITableView) {
        let hight = CGFloat(8) * CGFloat(250.0)
        self.tableh.constant = CGFloat(hight + 50)
        self.scroll.contentSize.height = CGFloat(hight + 200)
        //        print("Scroll height - \(CGFloat(hight + 700))")
        self.table.isScrollEnabled = false
    }
}
extension EventVc:FSCalendarDelegate {
  
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        self.date = self.formatter.string(from: date)
        self.table.reloadData()
        print("calendar did select date \(self.formatter.string(from: date))")
    }
}
