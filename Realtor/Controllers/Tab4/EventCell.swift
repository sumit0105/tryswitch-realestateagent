//
//  EventCell.swift
//  Realtor
//
//  Created by Anubhav on 02/11/21.
//

import UIKit

class EventCell: UITableViewCell {

    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var detailBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        detailBtn.setBorderColor(borderColor: .darkGray, cornerRadiusBound: 23
        )
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
