//
//  taskTitleVc.swift
//  Customer Service
//
//  Created by satyam mac on 25/11/21.
//

import UIKit

class taskTitleVc: UIViewController {

    @IBOutlet var ImagBorder: [UIImageView]!
    @IBOutlet weak var img1: UIImageView!
    @IBOutlet weak var img2: UIImageView!
    
    @IBOutlet weak var bottomCons: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()

        img1.image = UIImage(named: "check-1")
        let imagec = UIImage(named: "carve-arrow")
        img2.image = imagec?.changeTintColor(color: .black)
        for img in ImagBorder{
            img.setBorderColor(borderColor: .white, cornerRadiusBound: 17)
        }
        manageInputEventsForTheSubViews()
    }
    private func manageInputEventsForTheSubViews() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardFrameChangeNotfHandler(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardFrameChangeNotfHandler(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    @objc private func keyboardFrameChangeNotfHandler(_ notification: Notification) {
        
        if let userInfo = notification.userInfo {
            let keyboardFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
            let isKeyboardShowing = notification.name == UIResponder.keyboardWillShowNotification
            bottomCons.constant = isKeyboardShowing ? keyboardFrame.height  : 0
            UIView.animate(withDuration: 0, delay: 0, options: UIView.AnimationOptions.curveEaseOut, animations: {
                
                self.view.layoutIfNeeded()
            }, completion: { (completed) in
                
//                if isKeyboardShowing {
//                    let lastItem = self.chatsArray.count - 1
//                    let indexPath = IndexPath(item: lastItem, section: 0)
//                    self.chatCollView.scrollToItem(at: indexPath, at: .bottom, animated: true)
//                }
            })
        }
    }
    @IBAction func back_btn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

  

}
