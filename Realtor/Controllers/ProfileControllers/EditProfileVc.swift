//
//  EditProfileVc.swift
//  design
//
//  Created by satyam mac on 27/10/21.
//

import UIKit
protocol profileShow {
    func showDetails()
}
class EditProfileVc: UIViewController {
    @IBOutlet weak var bgView: UIView!
    
    @IBOutlet weak var hotsopView: UIView!
    var delegate:profileShow!
    override func viewDidLoad() {
        
        super.viewDidLoad()
        hotsopView.frame = self.view.frame
        self.view.addSubview(self.hotsopView)
        hotsopView.isHidden = true
        
        bgView.addShadow(shadowColor: UIColor.black, offSet: CGSize(width: 1, height: 1), opacity: 0.3, shadowRadius: 5.0)

       
    }
    @IBAction func backBtn(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func hotspot_Dismis(_ sender: Any) {
        self.hotsopView.isHidden = true
    }
    
    @IBAction func hotspot_Ok(_ sender: Any) {
        self.hotsopView.isHidden = true
        self.delegate.showDetails()
        self.navigationController?.popViewController(animated: false)
        
    }
    @IBAction func submitBtn(_ sender: UIButton) {
//        let vc = storyboard?.instantiateViewController(withIdentifier: "ProfileApprovalVc") as! ProfileApprovalVc
//        self.navigationController?.pushViewController(vc, animated:
//        false)
        self.hotsopView.isHidden = false
        
        
    }
    
}
