//
//  EdidVC1.swift
//  Real Estate Agent
//
//  Created by Yes IT Labs  on 31/12/21.
//

import UIKit
import UIKit
protocol profileShow1 {
   func showDetails()
}
class EdidVC1: UIViewController {

   
   
        @IBOutlet weak var bgView: UIView!
        
        @IBOutlet weak var hotsopView: UIView!
         var delegate:profileShow1!
        override func viewDidLoad() {
            
            super.viewDidLoad()
            hotsopView.frame = self.view.frame
            self.view.addSubview(self.hotsopView)
            hotsopView.isHidden = true
            
            bgView.addShadow(shadowColor: UIColor.black, offSet: CGSize(width: 1, height: 1), opacity: 0.3, shadowRadius: 5.0)

           
        }
        @IBAction func backBtn(_ sender: UIButton) {
            self.navigationController?.popViewController(animated: true)
        }

        @IBAction func hotspot_Dismis(_ sender: Any) {
            self.hotsopView.isHidden = true
        }
        
        @IBAction func hotspot_Ok(_ sender: Any) {
            self.hotsopView.isHidden = true
            self.delegate.showDetails()
            self.navigationController?.popViewController(animated: false)
            
        }
        @IBAction func submitBtn(_ sender: UIButton) {
    //        let vc = storyboard?.instantiateViewController(withIdentifier: "ProfileApprovalVc") as! ProfileApprovalVc
    //        self.navigationController?.pushViewController(vc, animated:
    //        false)
            self.hotsopView.isHidden = false
            
            
        }
        
    }
