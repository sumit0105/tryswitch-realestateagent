//
//  BuisnessInfoVc.swift
//  Realtor
//
//  Created by Anubhav on 03/11/21.
//

import UIKit

class BuisnessInfoVc: UIViewController {
    
    @IBOutlet var bgView: [UIView]!
    override func viewDidLoad() {
        super.viewDidLoad()
//        hotspotView.frame = self.view .frame
//        self.view.addSubview(self.hotspotView)
//        hotspotView.isHidden = true
        for item in bgView {
            
            item.addShadow(shadowColor: UIColor.black, offSet: CGSize(width: 1, height: 1), opacity: 0.3, shadowRadius: 5.0)
            if item.tag == 1{
                item.roundCornersBottomright_BottomLeft(radius: 20)
            }
        
        }
        
    }
    @IBAction func backBtn(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func changePassBtn(_ sender: UIButton) {
        let Story = UIStoryboard(name: AppStoryboard.PopUp.rawValue, bundle: nil)
        let vc = Story.instantiateViewController(withIdentifier: "ChangePasswordVc") as! ChangePasswordVc
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func editProfileBtn(_ sender: UIButton) {
//        let Story = UIStoryboard(name: AppStoryboard.PopUp.rawValue, bundle: nil)
//        let vc = Story.instantiateViewController(withIdentifier: "EditProfileVc") as! EditProfileVc
//        vc.delegate = self
//        self.navigationController?.pushViewController(vc, animated: true)
    }
//    func showDetails() {
//        self.hotspotView.isHidden = false
//    }
    
    
}


//extension UIView {
//
//    func addShadow(shadowColor: UIColor, offSet: CGSize, opacity: Float, shadowRadius: CGFloat) {
//
//        self.layer.shadowColor = shadowColor.cgColor
//        self.layer.shadowOffset = offSet
//        self.layer.shadowOpacity = opacity
//        self.layer.shadowRadius = shadowRadius
//    }
//}
