//
//  ChatVc.swift
//  design
//
//  Created by satyam mac on 08/11/21.
//

import UIKit
import DropDown
class ChatVc1: UIViewController {
    @IBOutlet weak var txt_TypeId:UITextField!
    @IBOutlet weak var tableV:UITableView!
    @IBOutlet var inputViewContainerBottomConstraint: NSLayoutConstraint!
    
    let drop_IdType = DropDown()
    
    lazy var dropDowns: [DropDown] = {
        return [
            self.drop_IdType
        ]
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        tableV.delegate = self
        tableV.dataSource = self
        tableV.register(UINib(nibName: "MassageRcell", bundle: nil), forCellReuseIdentifier: "MassageRcell")
        tableV.register(UINib(nibName: "MessegeSentCell", bundle: nil), forCellReuseIdentifier: "MessegeSentCell")
        tableV.register(UINib(nibName: "sentMsgeCell2", bundle: nil), forCellReuseIdentifier: "sentMsgeCell2")
        
     
        self.manageInputEventsForTheSubViews()
    }
    @IBAction func back_btn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    private func manageInputEventsForTheSubViews() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardFrameChangeNotfHandler(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardFrameChangeNotfHandler(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    @objc private func keyboardFrameChangeNotfHandler(_ notification: Notification) {
        
        if let userInfo = notification.userInfo {
            let keyboardFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
            let isKeyboardShowing = notification.name == UIResponder.keyboardWillShowNotification
            inputViewContainerBottomConstraint.constant = isKeyboardShowing ? keyboardFrame.height  : 0
            UIView.animate(withDuration: 0, delay: 0, options: UIView.AnimationOptions.curveEaseOut, animations: {
                
                self.view.layoutIfNeeded()
            }, completion: { (completed) in
                
//                if isKeyboardShowing {
//                    let lastItem = self.chatsArray.count - 1
//                    let indexPath = IndexPath(item: lastItem, section: 0)
//                    self.chatCollView.scrollToItem(at: indexPath, at: .bottom, animated: true)
//                }
            })
        }
    }
    @IBAction func btn_selectTypeID(_ sender: UIButton){
        
        self.drop_IdType.anchorView = sender
        self.drop_IdType.bottomOffset = CGPoint(x: 0, y: sender.bounds.height - 4)
        self.drop_IdType.textColor = .black
//        self.drop_IdType.s
        self.drop_IdType.separatorColor = .clear
        self.drop_IdType.selectionBackgroundColor = .clear
        self.drop_IdType.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.drop_IdType.dataSource.removeAll()
        drop_IdType.cornerRadius = 12
        self.drop_IdType.cellHeight = 35
        self.drop_IdType.dataSource.append(contentsOf: ["Accept","Deny"])
        
        self.drop_IdType.selectionAction = { [unowned self] (index, item) in
            self.txt_TypeId.text = item
        }
        self.drop_IdType.show()
        
    }
}
    extension ChatVc1:UITableViewDelegate,UITableViewDataSource{
        func tableView (_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return 3
        }
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
            if indexPath.row == 0{
                let cell = tableView.dequeueReusableCell(withIdentifier: "MessegeSentCell") as! MessegeSentCell
                cell.selectionStyle = .none
                return cell
               
            }else if indexPath.row == 1{
                let cell = tableView.dequeueReusableCell(withIdentifier: "MassageRcell") as! MassageRcell
                cell.selectionStyle = .none
                return cell
            }else if indexPath.row == 2{
                let cell = tableView.dequeueReusableCell(withIdentifier: "sentMsgeCell2") as! sentMsgeCell2
                cell.selectionStyle = .none
                return cell
            }
          
            return UITableViewCell()
            
        }
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            UITableView.automaticDimension
        }
        
    }



