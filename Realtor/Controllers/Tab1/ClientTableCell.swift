//
//  ClientTableCell.swift
//  Realtor
//
//  Created by Anubhav on 01/11/21.
//

import UIKit

class ClientTableCell: UITableViewCell {

    @IBOutlet weak var lbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        lbl.roundCorners(radius: 10)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
