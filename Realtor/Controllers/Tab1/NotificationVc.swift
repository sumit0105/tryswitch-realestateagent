//
//  NotificationVc.swift
//  design
//
//  Created by satyam mac on 08/11/21.
//

import UIKit

class NotificationVc: UIViewController {
    @IBOutlet weak var tableV:UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        tableV.delegate = self
        tableV.dataSource = self
        tableV.register(UINib(nibName: "NotificationCell", bundle: nil), forCellReuseIdentifier: "NotificationCell")
        

    }
    
    @IBAction func back_btn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
extension NotificationVc:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell") as! NotificationCell
        if indexPath.row % 2 == 0{
            cell.backgroundColor = UIColor.init(red: 243, green: 247, blue: 255)
        }else{
            cell.backgroundColor = UIColor.init(red: 222, green: 230, blue: 245)
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        203
    }
    
}
