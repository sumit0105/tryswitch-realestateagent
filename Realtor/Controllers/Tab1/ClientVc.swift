//
//  ClientVcViewController.swift
//  Realtor
//
//  Created by Anubhav on 01/11/21.
//

import UIKit

class ClientVc:UIViewController {
    @IBOutlet weak var TbleVw:UITableView!
    @IBOutlet var hotspotViw: UIView!
    @IBOutlet weak var invitClinentVie: UIView!
    @IBOutlet weak var bottomAddView: UIView!
    @IBOutlet weak var hPlusView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.hotspotViw.frame = self.view.frame
                self.view.addSubview(self.hotspotViw)
                self.hotspotViw.isHidden = true
        hPlusView.setBorderColor(borderColor: .black, cornerRadiusBound: 23)
        let gesture = UITapGestureRecognizer(target: self, action:  #selector (inviteAction (_:)))
        invitClinentVie.isUserInteractionEnabled = true
        invitClinentVie.addGestureRecognizer(gesture)
        
        
        TbleVw.delegate = self
        TbleVw.dataSource = self
        TbleVw.register(UINib(nibName: "ClientTableCell", bundle: nil), forCellReuseIdentifier: "ClientTableCell")
        bottomAddView.roundCorners(radius: 23)
       
    }
    @IBAction func btnInvite(_ sender: Any) {
        self.hotspotViw.isHidden = true
        let vc = ClientListVcViewController.instantiate(fromAppStoryboard: .SignUp)
        vc.hidesBottomBarWhenPushed = false
       
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func btn_Add(_ sender: UIButton) {
        self.hotspotViw.isHidden = false
    }
    
    @IBAction func dismisBtn(_ sender: Any) {
        self.hotspotViw.isHidden = true
    }
    @objc func inviteAction(_ sender:UITapGestureRecognizer){
        self.hotspotViw.isHidden = false
       }
    @IBAction func back_btn(_ sender: Any) {
        self.tabBarController?.selectedIndex = 4
    }
    
    @IBAction func notification_btn(_ sender: Any) {
        let vc = NotificationVc.instantiate(fromAppStoryboard: .SignUp)
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
extension ClientVc:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return 8
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ClientTableCell") as! ClientTableCell
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       
        120
    }
    
    
}
