//
//  ClientListVcViewController.swift
//  Realtor
//
//  Created by Anubhav on 10/11/21.
//

import UIKit

class ClientListVcViewController: UIViewController {

    @IBOutlet var borderView: [UIView]!
    
   
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var scroll: UIScrollView!
    @IBOutlet weak var tableh : NSLayoutConstraint!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        for bg in borderView{
            bg.setBorderColor(borderColor: UIColor.init(red: 68, green: 0, blue: 99), cornerRadiusBound: 20)
        }
        self.tabBarController?.tabBar.isHidden = false
        self.tableView.register(UINib(nibName: "ClientListCell", bundle: nil), forCellReuseIdentifier: "ClientListCell")
    }
    @objc func inviteAction(_ sender:UITapGestureRecognizer){
        EventBottomVc.openDemo(from: self, in: self.view)
       }
    @IBAction func btn_HomeTap(_ sender: UIButton) {
       
    }
 
    
    @IBAction func btn_BackTap(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension ClientListVcViewController :UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 8
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//
////        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 30))
////        headerView.backgroundColor = .clear
////        let label = UILabel()
////        label.frame = CGRect.init(x: 0, y: 0, width: headerView.frame.width-20, height: headerView.frame.height - 10)
////
//////        label.text = "Other Products sold by Robert Adler"
////        label.text = "Autres produits/services par Robert Adler"
////
////        label.textColor = UIColor.black
////        label.font = UIFont(name: AppFonts.Roboto_Medium.rawValue, size: 14.0)
////
////        headerView.addSubview(label)
//
////        return headerView
//    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ClientListCell", for: indexPath as IndexPath) as!  ClientListCell

//        let cell = table.dequeueReusableCell(withIdentifier: "ClientListCell", for: indexPath) as! ClientListCell
       updateHight(table: tableView)
        cell.selectionStyle = .none
        return cell
    }
    
        


    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
    }
    
    func updateHight(table:UITableView) {
        let hight = CGFloat(8) * CGFloat(80.0)
        self.tableh.constant = CGFloat(hight + 50)
        self.scroll.contentSize.height = CGFloat(hight + 300)
        //        print("Scroll height - \(CGFloat(hight + 700))")
        self.tableView.isScrollEnabled = false
    }
}
